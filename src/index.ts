var arr = [0, 1, 2, 3].reverse(); 
console.log("Reversed array is : " + arr );
function swap(arr: number[], x: n, y:n): void {
  const temp = arr[x];
  arr[x] = arr[y];
  arr[y] = temp;
}

function reverse(arr: number[]): void{
 for(var i = 0; i < arr.length / 2; i += 1) swap(arr, i,arr.length - i - 1);
}

function flatten(arr)